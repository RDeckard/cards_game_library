# A specialised subclass of the OpenCard class for classic game cards.
# Give an easier .new method, and an appropriate #name method.
class ClassicCard < OpenCard
  # Color attribute for classic cards game.
  class Color < OpenCard::Attribute
    open_card_attribute_data(
      {
        'S' => {
          name: 'Spades',
          rank: 1
        },
        'H' => {
          name: 'Hearts',
          rank: 2
        },
        'D' => {
          name: 'Diamonds',
          rank: 3
        },
        'C' => {
          name:  'Clubs',
          rank: 4
        }
      }.freeze
    )
  end

  # Value attribute for classic cards game.
  class Value < OpenCard::Attribute
    open_card_attribute_data(
      (2..10)
      .map { |v| [v, { name: v.to_s, rank: v }] }.to_h
      .merge(
        'J' => {
          name: 'Jack',
          rank: 11
        },
        'Q' => {
          name: 'Queen',
          rank: 12
        },
        'K' => {
          name: 'King',
          rank: 13
        },
        'A' => {
          name: 'Ace',
          rank: 14
        }
      ).freeze
    )
  end

  # @param [String, Integer] value the code for the card value
  #   (ex: 2, 7, 10, 'Q', 'A', etc.)
  # @param [String] color the code for the card color
  #   ('S', 'H', 'D' or 'C')
  def initialize(value, color)
    super value: Value.new(value),
          color: Color.new(color)
  end

  # @return [String] an appropriate representation of self
  def name
    "#{value.name} of #{color.name}"
  end
end
