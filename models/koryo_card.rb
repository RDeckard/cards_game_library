# A specialised subclass of the OpenCard class for Koryo game cards.
# Give an easier .new method, and an appropriate #name method.
class KoryoCard < OpenCard
  # Type attribute for Koryo cards game
  class Type < OpenCard::Attribute
    open_card_attribute_data(
      {
        omniscient: {
          type: :character,
          name: 'Omniscient',
          rank: 1
        },
        spy: {
          type: :character,
          name: 'Spy',
          rank: 2
        },
        senator: {
          type: :character,
          name: 'Senator',
          rank: 3
        },
        priest: {
          type: :character,
          name: 'Priest',
          rank: 4
        },
        ship_owner: {
          type: :character,
          name: 'Ship Owner',
          rank: 5
        },
        banker: {
          type: :character,
          name: 'Banker',
          rank: 6
        },
        guardian: {
          type: :character,
          name: 'Guardian',
          rank: 7
        },
        broadcaster: {
          type: :character,
          name: 'Broadcaster',
          rank: 8
        },
        merchant: {
          type: :character,
          name: 'Merchant',
          rank: 9
        },
        barbarian: {
          type: :event,
          name: 'Barbarian'
        },
        lobbying: {
          type: :event,
          name: 'Lobbying'
        }
      }.freeze
    )
  end

  # @param [Symbol] type the name (or rank) of the card
  #   (ex: :merchant, :ship_owner, :lobbying, etc.)
  def initialize(type)
    super type: Type.new(type)
  end

  # @return [String] an appropriate representation of self
  def name
    %|#{type.type}: #{type.name}#{" (#{type.rank})" if type.rank}|
  end
end
