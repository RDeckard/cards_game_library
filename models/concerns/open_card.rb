# An OpenCard instance can take an arbitrary number of OpenCard::Attribute's
# subclasses instances and give basic introspections methods.
class OpenCard < OpenStruct
  # A template for arbitrary open card attribute classes.
  class Attribute
    class << self
      attr_reader :data
    end
    attr_reader :code

    # A DSL method for the initialization of `data` (class attribute).
    # @param [Hash] data a Hash like this:
    #   {
    #     'S' => {
    #       name: 'Spades',
    #       optional_key1: 1,
    #       optional_key2: 'foo',
    #       ...
    #     },
    #     'H' => {
    #       name: 'Hearts',
    #       optional_key1: 2,
    #       ...
    #     },
    #     ...
    #   }
    def self.open_card_attribute_data(data)
      @data = data

      self.data.values.flat_map(&:keys).uniq.each do |data_key|
        define_method data_key do
          self.class.data.fetch(code)[data_key]
        end
      end
    end

    # @!attribute [r] code
    #   @return the code saved at initialization

    # @param code must be a key of the hash given to open_card_attribute_data
    def initialize(code)
      unless self.class.data.keys.include?(code)
        raise "'#{code}' is not a key of the hash returned by #{self.class}.data"
      end
      @code = code
    end
  end

  # @return [String] the codes concatenation of each attributes
  def code
    to_h
      .values
      .map(&:code)
      .join
  end

  # @return [String] a concatenation of general presentations of each attributes
  def name
    to_h.map do |key, value|
      "#{key}: #{value.name}"
    end.join(', ')
  end
end
