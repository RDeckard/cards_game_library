# A specialised subclass of the OpenCard class for Love Letter game cards.
# Give an easier .new method, and an appropriate #name method.
class LoveLetterCard < OpenCard
  # Type attribute for Love Letter cards game
  class Type < OpenCard::Attribute
    open_card_attribute_data(
      {
        guard: {
          name: 'Guard',
          rank: 1
        },
        priest: {
          name: 'Priest',
          rank: 2
        },
        baron: {
          name: 'Baron',
          rank: 3
        },
        handmaid: {
          name: 'Handmaid',
          rank: 4
        },
        prince: {
          name: 'Prince',
          rank: 5
        },
        king: {
          name: 'King',
          rank: 6
        },
        countess: {
          name: 'Countess',
          rank: 7
        },
        princess: {
          name: 'Princess',
          rank: 8
        }
      }.freeze
    )
  end

  # @param [Symbol] type the name (or rank) of the card
  #   (ex: :baron, :prince, etc.)
  def initialize(type)
    super type: Type.new(type)
  end

  # @return [String] an appropriate representation of self
  def name
    "#{type.name} (#{type.rank})"
  end
end
