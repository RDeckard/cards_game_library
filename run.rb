require 'ostruct'
require_relative 'models/models'

### GENERATE DECKS ###

card_decks = {}

# CLASSIC CARDS (alongside with raw Open Cards counterparts)
card_decks[:open]     = []
card_decks[:classic]  = []
ClassicCard::Color.data.keys.flat_map do |color|
  ClassicCard::Value.data.keys.map do |value|
    card_decks[:classic] << ClassicCard.new(value, color)
    card_decks[:open] << OpenCard.new(
      value: ClassicCard::Value.new(value),
      color: ClassicCard::Color.new(color)
    )
  end
end

# LOVE LETTER CARDS
card_decks[:love_letter] = []
card_decks[:love_letter] += Array.new(5) { LoveLetterCard.new(:guard)    }
card_decks[:love_letter] += Array.new(2) { LoveLetterCard.new(:priest)   }
card_decks[:love_letter] += Array.new(2) { LoveLetterCard.new(:baron)    }
card_decks[:love_letter] += Array.new(2) { LoveLetterCard.new(:handmaid) }
card_decks[:love_letter] += Array.new(2) { LoveLetterCard.new(:prince)   }
card_decks[:love_letter] += Array.new(1) { LoveLetterCard.new(:king)     }
card_decks[:love_letter] += Array.new(1) { LoveLetterCard.new(:countess) }
card_decks[:love_letter] += Array.new(1) { LoveLetterCard.new(:princess) }

# KORYO CARDS
card_decks[:koryo] = []
card_decks[:koryo] += Array.new(9) { KoryoCard.new(:merchant)    }
card_decks[:koryo] += Array.new(8) { KoryoCard.new(:broadcaster) }
card_decks[:koryo] += Array.new(7) { KoryoCard.new(:guardian)    }
card_decks[:koryo] += Array.new(6) { KoryoCard.new(:banker)      }
card_decks[:koryo] += Array.new(5) { KoryoCard.new(:ship_owner)  }
card_decks[:koryo] += Array.new(4) { KoryoCard.new(:priest)      }
card_decks[:koryo] += Array.new(3) { KoryoCard.new(:senator)     }
card_decks[:koryo] += Array.new(2) { KoryoCard.new(:spy)         }
card_decks[:koryo] << Array.new(1) { KoryoCard.new(:omniscient)  }
card_decks[:koryo] += Array.new(6) { KoryoCard.new(:barbarian)   }
card_decks[:koryo] += Array.new(4) { KoryoCard.new(:lobbying)    }

### DISPLAY DECKS' SAMPLES ###

card_decks.each do |name, deck|
  puts "Samples of #{name.to_s.gsub('_', ' ').capitalize} cards: (#{deck.size})"
  deck.sample(3).each_with_index do |card, i|
    puts "  sample #{i + 1}:", "    CODE: '#{card.code}'", "    NAME: \"#{card.name}\""
  end
end
